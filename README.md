# Ansible Role: JIRA & BITBUCKET BACKUPS
###An Ansible Role that performs:
## Backup of Jira used by KOI project

      NOTE: JIRA must be backed up with a longer than 2 days interval due to internal Atlassian rules
      Information here: 
      Revelant part: 48 hour delay for backups including attachments
      You can perform data-only backups—excluding attachments, avatars, and logos—one after the other, without a delay. *When you're creating a backup that includes attachments, avatars, and logos, though, you need to wait 48 hours from the when you last created a backup.*
      * Used script backup-jira-api-token.sh from [Atlassian Labs](https://bitbucket.org/atlassianlabs/automatic-cloud-backup.git)
            
## Clone of all projects in which airgeral is member
# Step by Step install
## Generate keys for ssh

      ssh-keygen

      Generating public/private rsa key pair.
      Enter file in which to save the key (/root/.ssh/id_rsa):
      Created directory '/root/.ssh'.
      Enter passphrase (empty for no passphrase):
      Enter same passphrase again:
      Your identification has been saved in /root/.ssh/id_rsa.
      Your public key has been saved in /root/.ssh/id_rsa.pub.
      The key fingerprint is:
      SHA256:4OmeF5Kocqj6i4DPEL2L64D/7THjIBwJZpwbA//Wokw root@testing.ad.airc.pt
      The key's randomart image is:
      +---[RSA 2048]----+
      |.                |
      |o..              |
      |.B.   .          |
      |oo+o o o         |
      |..E +.+.S        |
      |o= =.oo .        |
      |*.*.. =. .       |
      |=Boo = =.        |
      |X*Bo..*.         |
      +----[SHA256]-----+


There will be two files on the /root/.ssh:

      id_rsa id_rsa.pub

Add the private key 

    ssh-add /root/.ssh/id_rsa

Add the public key to your Bitbucket settings

    cat /root/.ssh/id_rsa.pub

Select and copy the value

From Bitbucket, choose Bitbucket settings from your avatar in the lower left (must be an user with permissions to the required repositories).
  The Account settings page opens.
  Click SSH KEYS

From Bitbucket, click Add key

Enter a Label for your new key, for example, Backup public key.
Paste the copied public key into the SSH Key field.
You may see an email address on the last line when you paste. It doesn't matter whether or not you include the email address in the Key.
Click Save.
Bitbucket sends you an email to confirm the addition of the key.

Validate that you can access via ssh

      [root@testing backup_JIRA_BITBUCKET]# ssh -T git@bitbucket.org
      The authenticity of host 'bitbucket.org (18.205.93.2)' can't be established.
      RSA key fingerprint is SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A.
      RSA key fingerprint is MD5:97:8c:1b:f2:6f:14:6b:5c:3b:ec:aa:46:46:74:7c:40.
      Are you sure you want to continue connecting (yes/no)? yes
      Warning: Permanently added 'bitbucket.org,18.205.93.2' (RSA) to the list of known hosts.
      logged in as aircgeral.

      You can use git or hg to connect to Bitbucket. Shell access is disabled.
      
The first time it will be asked for the continue connecting, answer yes.

## Install required tools


    yum install ansible git -y


## Clone repository by using the comand below and enter in it:


    cd /tmp
    git clone https://bitbucket.org/aircman/backup_jira_bitbucket.git
    cd backup_jira_bitbucket/ansible


## Executing the playbook


    ansible-playbook backup_jira_bitbucket.yml -c local 


# TODO

## Configure all the required vars

    # vars to be used
    # BitBucket related
    bitbucket_email: geral@airc.pt
    bitbucket_token: i0JFE1Eh5e3rXnNuJt947F68
    bitbucket_host: aircpt.atlassian.net
    bitbucket_backupdir: /backup/jira
    # Backup GIT
    git_outfile: /tmp/file$$
    git_repolist: /tmp/repo$$
    git_tempdir: /tmp/git
    git_backupdir: /backup/git
    git_allrepofile: allrepo
