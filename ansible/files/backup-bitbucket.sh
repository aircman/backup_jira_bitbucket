export outfile=/tmp/file$$
export repolist=/tmp/repo$$
export tempdir=/tmp/git
export backupdir=/backup/git
export allrepofile=allrepo
export today=$(TZ=$TIMEZONE date +%d-%m-%Y)

# user aircgeral tem que ser membro dos repositórios a salvaguardar

rm -rf $tempdir

curl -X GET   'https://api.bitbucket.org/2.0/repositories?role=member&pagelen=100' -H 'Authorization: Basic YWlyY2dlcmFsOmFpcmMjIzAw' -H 'cache-control: no-cache' > $outfile

jq -r '.values | .[] | .links | .clone | .[1] | .href' $outfile > $repolist

mkdir -p $tempdir

cd $tempdir

while read repo; do
    git clone "$repo"
done < $repolist

tar cvfj $backupdir/$allrepofile$today.tar.bz2 .

rm -f $outfile $repolist


